import { Component, OnInit } from '@angular/core';
import {ProductoService} from '../productos/services/producto.service';


@Component({
  selector: 'app-productos-form',
  templateUrl: './productos-form.component.html',
  styleUrls: ['./productos-form.component.scss'],
})

export class ProductosFormComponent implements OnInit {

  constructor(private serviceProducto: ProductoService) { }

  ngOnInit() {
  }

}
