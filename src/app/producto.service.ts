import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Producto} from './productos/model/producto';

@Injectable()
export class ProductoService {
  productoSelected: Producto;
  productos: Producto[];
  readonly URL = 'http:localhost:8080/productos'
  constructor(private http: HttpClient) {
    this.productoSelected = new Producto();
  }

  getProductos() {
    return this.http.get(this.URL);
  }
}

