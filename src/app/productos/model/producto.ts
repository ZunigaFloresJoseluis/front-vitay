export class Producto {

    Index: string;
    CreatedAt: string;
    UpdatedAt: string;
    DeletedAt: string;
    Id: string;
    IdDescuento: string;
    IdUnidad: string;
    Nombre: string;
    Descripcion: string;
    Imagen: string;
    Precio: number;
    DisponibleVenta: boolean;
    TipoProducto: number;
    Impuesto: string;
    Categoria: string;
    Impuestos: string;
    Servicios: string;
    Categorias: string;
    Estado: string;

    constructor(IdUnidad = '', Nombre = '', Descripcion = '', Imagen = '', Precio = 0, DisponibleVenta = false, TipoProducto = 0) {
        this.IdUnidad = IdUnidad;
        this.Nombre = Nombre;
        this.Descripcion = Descripcion;
        this.Imagen = Imagen;
        this.Precio = Precio;
        this.DisponibleVenta = DisponibleVenta;
        this.TipoProducto = TipoProducto;
    }

}
