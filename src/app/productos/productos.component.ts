import { Component, OnInit } from '@angular/core';
import {ProductoService} from './services/producto.service';

import {Response} from './model/response';
import {Producto} from './model/producto';
@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss'],
  providers: [ProductoService]

})
export class ProductosComponent implements OnInit {
  response: Response;
  constructor(private serviceProducto: ProductoService) { }

  ngOnInit() {
    this.getAllProductos();
  }

  getAllProductos() {
    this.serviceProducto.getProductos()
        .subscribe(resp => {
          this.response = resp as Response;
          this.serviceProducto.productos = this.response.Data as Producto[];
        });
  }

}
