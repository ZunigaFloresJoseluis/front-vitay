import { Injectable } from '@angular/core';
import {Producto} from '../model/producto';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ProductoService {
  productoSelected: Producto;
  productos: Producto[];
  readonly URL = 'http://localhost:8080/productos';
  constructor(private http: HttpClient) {
    this.productoSelected = new Producto();
  }

  getProductos() {
    return this.http.get(this.URL);
  }

  postProducto(producto: Producto) {
    return this.http.post(this.URL, producto);
  }
}
